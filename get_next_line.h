/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 16:03:36 by jwong             #+#    #+#             */
/*   Updated: 2016/03/16 14:16:11 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef H_GET_NEXT_LINE_H
# define H_GET_NEXT_LINE_H

# define BUFF_SIZE 32

int	get_next_line(const int fd, char **line);

#endif
