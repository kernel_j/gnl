/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jwong <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/03/02 16:06:00 by jwong             #+#    #+#             */
/*   Updated: 2016/03/02 18:35:41 by jwong            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
//#include "libft/includes/libft.h"
#include "get_next_line.h"

int		main(int argc, char **argv)
{
	int		i;
	int		fd;
	int		ret;
	char	*line;

	if (argc < 1)
		return (-1);
	else
	{
        if (argc <= 1)
        {
            while ((ret = get_next_line(0, &line)) > 0)
            {
                printf("%s....%d\n", line, ret);
            }
            printf("%s....%d\n", line, ret);
        }
        else 
        {
            i = 1;
            while (i < argc)
            {
                fd = open(argv[i], O_RDONLY);
                ret = 1;
                while ((ret = get_next_line(fd, &line)) > 0)
                    printf("%s....%d\n", line, ret);
                printf("%s....%d\n", line, ret);
                i++;
            }
        }
    }
    return (0);
}
